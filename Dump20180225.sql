-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dbCotVac
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AvailableOptions`
--

DROP TABLE IF EXISTS `AvailableOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AvailableOptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GroupsId` int(11) DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PersonsFrom` int(11) NOT NULL,
  `PersonsTo` int(11) NOT NULL,
  `NightsFrom` int(11) NOT NULL,
  `NightsTo` int(11) NOT NULL,
  `Price` decimal(16,2) NOT NULL,
  `OrderRank` int(11) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AvailableOptions`
--

LOCK TABLES `AvailableOptions` WRITE;
/*!40000 ALTER TABLE `AvailableOptions` DISABLE KEYS */;
INSERT INTO `AvailableOptions` VALUES (1,1,'Cantidad de Personas',NULL,'int',1,9,1,1,475.00,50,1,NULL,NULL),(2,1,'Cantidad de Personas',NULL,'int',10,14,1,1,425.00,50,1,NULL,NULL),(3,1,'Cantidad de Personas',NULL,'int',15,999,1,1,375.00,50,1,NULL,NULL),(4,1,'Cantidad de Personas',NULL,'int',1,9,2,2,450.00,50,1,NULL,NULL),(5,1,'Cantidad de Personas',NULL,'int',10,14,2,2,400.00,50,1,NULL,NULL),(6,1,'Cantidad de Personas',NULL,'int',15,999,2,2,350.00,50,1,NULL,NULL),(7,1,'Cantidad de Personas',NULL,'int',1,9,3,99,450.00,50,1,NULL,NULL),(8,1,'Cantidad de Personas',NULL,'int',10,14,3,99,400.00,50,1,NULL,NULL),(9,1,'Cantidad de Personas',NULL,'int',15,999,3,99,350.00,50,1,NULL,NULL),(10,2,'Cantidad de Noches',NULL,'int',0,9999,0,9999,0.00,60,1,NULL,NULL),(11,NULL,'Pisina',NULL,'bool',0,9999,0,9999,2000.00,300,1,NULL,NULL),(12,NULL,'Kareokee',NULL,'bool',0,9999,0,9999,2800.00,310,1,NULL,NULL),(13,3,'Apartamento Alquilado',NULL,'select',0,9999,0,9999,2167.00,686,1,NULL,NULL),(14,3,'Lugar Abierto Para Camping',NULL,'select',0,9999,0,9999,2702.00,821,1,NULL,NULL),(15,4,'Sin Comida',NULL,'select',30,9999,0,9999,0.00,400,1,NULL,NULL),(16,4,'Renta de Cosina',NULL,'select',0,9999,0,9999,1500.00,410,1,NULL,NULL),(17,4,'Desayuno y Cena',NULL,'select',0,9999,0,9999,2500.00,420,1,NULL,NULL),(18,4,'Desayuno, Cena y Comida',NULL,'select',0,9999,0,9999,3500.00,430,1,NULL,NULL),(19,4,'Desayuno, Cena y Comida',NULL,'select',0,9999,0,9999,4200.00,440,1,NULL,NULL);
/*!40000 ALTER TABLE `AvailableOptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES (1,'Personas',NULL,1,NULL,NULL),(2,'Noches',NULL,1,NULL,NULL),(3,'Plan de Campamento',NULL,1,NULL,NULL),(4,'Plan de Comida',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Quotations`
--

DROP TABLE IF EXISTS `Quotations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Quotations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Celular` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Confirmed` tinyint(1) DEFAULT NULL,
  `TaxesPorc` decimal(4,2) NOT NULL,
  `Taxes` decimal(16,2) NOT NULL,
  `SubTotal` decimal(16,2) NOT NULL,
  `Total` decimal(16,2) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Quotations`
--

LOCK TABLES `Quotations` WRITE;
/*!40000 ALTER TABLE `Quotations` DISABLE KEYS */;
INSERT INTO `Quotations` VALUES (1,'Eduardo','Santana',NULL,'8093157934','eduardo_amparo@hotmail.com',NULL,0.18,5871.06,32617.00,38488.06,1,'2018-01-31 01:38:55','2018-01-31 01:38:55'),(2,'Eduardo Amparo','Santana Severino',NULL,'8093142323','me2@me.com',NULL,0.18,5886.36,32702.00,38588.36,1,'2018-02-03 22:00:44','2018-02-03 22:00:44'),(3,'Eduardo Amparo','Santana Severino',NULL,'8093142323','me2@me.com',NULL,0.18,5886.36,32702.00,38588.36,1,'2018-02-03 22:01:18','2018-02-03 22:01:18'),(4,'Eduardo Amparo','Santana Severino',NULL,'8093142323','me2@me.com',NULL,0.18,5886.36,32702.00,38588.36,1,'2018-02-03 22:02:49','2018-02-03 22:02:49'),(5,'Ramon En','Santana',NULL,'809456789','raymong@me.com',NULL,0.18,6642.36,36902.00,43544.36,1,'2018-02-03 22:06:12','2018-02-03 22:06:12'),(6,'Eduardo','Santana',NULL,'8093157934','me@me.com',NULL,0.18,6249.06,34717.00,40966.06,1,'2018-02-17 19:01:32','2018-02-17 19:01:32'),(7,'Eduardo','Santana',NULL,'8093157934','me@me.com',NULL,0.18,6249.06,34717.00,40966.06,1,'2018-02-17 19:01:47','2018-02-17 19:01:47'),(8,'Eduardo','Santana',NULL,'8095675656','me@me.com',NULL,0.18,6147.36,34152.00,40299.36,1,'2018-02-17 19:10:42','2018-02-17 19:10:42'),(9,'Eduardo','Santana',NULL,'8095675656','me@me.com',NULL,0.18,6147.36,34152.00,40299.36,1,'2018-02-17 19:16:13','2018-02-17 19:16:13'),(10,'Ramona','Severino',NULL,'8094569878','pera@me.com',NULL,0.18,6327.36,35152.00,41479.36,1,'2018-02-17 19:17:45','2018-02-17 19:17:45'),(11,'Marisela','Quintanilla',NULL,'8095647878','45elabispao@me.com',NULL,0.18,34686.36,192702.00,227388.36,1,'2018-02-17 19:21:37','2018-02-17 19:21:37');
/*!40000 ALTER TABLE `Quotations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QuotationsLines`
--

DROP TABLE IF EXISTS `QuotationsLines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QuotationsLines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `QuotationsId` int(11) NOT NULL,
  `AvailableOptionsId` int(11) NOT NULL,
  `SelectedValue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GroupsId` int(11) DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GroupName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Qty` int(11) NOT NULL,
  `Price` decimal(16,2) NOT NULL,
  `Total` decimal(16,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QuotationsLines`
--

LOCK TABLES `QuotationsLines` WRITE;
/*!40000 ALTER TABLE `QuotationsLines` DISABLE KEYS */;
INSERT INTO `QuotationsLines` VALUES (1,4,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-03 22:02:49','2018-02-03 22:02:49'),(2,4,12,'true',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-03 22:02:49','2018-02-03 22:02:49'),(3,4,9,'20',1,'Cantidad de Personas','Personas',20,350.00,7000.00,'2018-02-03 22:02:49','2018-02-03 22:02:49'),(4,4,10,'3',2,'Cantidad de Noches','Noches',3,0.00,0.00,'2018-02-03 22:02:49','2018-02-03 22:02:49'),(5,4,14,'14',3,'Lugar Abierto Para Camping','Plan de Campamento',1,2702.00,2702.00,'2018-02-03 22:02:49','2018-02-03 22:02:49'),(6,4,19,'19',4,'Desayuno, Cena y Comida','Plan de Comida',1,4200.00,4200.00,'2018-02-03 22:02:49','2018-02-03 22:02:49'),(7,5,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-03 22:06:12','2018-02-03 22:06:12'),(8,5,12,'false',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-03 22:06:12','2018-02-03 22:06:12'),(9,5,6,'33',1,'Cantidad de Personas','Personas',33,350.00,11550.00,'2018-02-03 22:06:12','2018-02-03 22:06:12'),(10,5,10,'2',2,'Cantidad de Noches','Noches',2,0.00,0.00,'2018-02-03 22:06:12','2018-02-03 22:06:12'),(11,5,14,'14',3,'Lugar Abierto Para Camping','Plan de Campamento',1,2702.00,2702.00,'2018-02-03 22:06:12','2018-02-03 22:06:12'),(12,5,17,'17',4,'Desayuno y Cena','Plan de Comida',1,2500.00,2500.00,'2018-02-03 22:06:12','2018-02-03 22:06:12'),(13,6,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-17 19:01:32','2018-02-17 19:01:32'),(14,6,12,'true',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-17 19:01:32','2018-02-17 19:01:32'),(15,6,9,'25',1,'Cantidad de Personas','Personas',25,350.00,8750.00,'2018-02-17 19:01:32','2018-02-17 19:01:32'),(16,6,10,'3',2,'Cantidad de Noches','Noches',3,0.00,0.00,'2018-02-17 19:01:32','2018-02-17 19:01:32'),(17,6,13,'13',3,'Apartamento Alquilado','Plan de Campamento',1,2167.00,2167.00,'2018-02-17 19:01:32','2018-02-17 19:01:32'),(18,6,16,'16',4,'Renta de Cosina','Plan de Comida',1,1500.00,1500.00,'2018-02-17 19:01:32','2018-02-17 19:01:32'),(19,7,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-17 19:01:47','2018-02-17 19:01:47'),(20,7,12,'true',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-17 19:01:47','2018-02-17 19:01:47'),(21,7,9,'25',1,'Cantidad de Personas','Personas',25,350.00,8750.00,'2018-02-17 19:01:47','2018-02-17 19:01:47'),(22,7,10,'3',2,'Cantidad de Noches','Noches',3,0.00,0.00,'2018-02-17 19:01:47','2018-02-17 19:01:47'),(23,7,13,'13',3,'Apartamento Alquilado','Plan de Campamento',1,2167.00,2167.00,'2018-02-17 19:01:47','2018-02-17 19:01:47'),(24,7,16,'16',4,'Renta de Cosina','Plan de Comida',1,1500.00,1500.00,'2018-02-17 19:01:47','2018-02-17 19:01:47'),(25,8,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-17 19:10:42','2018-02-17 19:10:42'),(26,8,12,'true',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-17 19:10:42','2018-02-17 19:10:42'),(27,8,9,'23',1,'Cantidad de Personas','Personas',23,350.00,8050.00,'2018-02-17 19:10:42','2018-02-17 19:10:42'),(28,8,10,'3',2,'Cantidad de Noches','Noches',3,0.00,0.00,'2018-02-17 19:10:42','2018-02-17 19:10:42'),(29,8,14,'14',3,'Lugar Abierto Para Camping','Plan de Campamento',1,2702.00,2702.00,'2018-02-17 19:10:42','2018-02-17 19:10:42'),(30,8,17,'17',4,'Desayuno y Cena','Plan de Comida',1,2500.00,2500.00,'2018-02-17 19:10:42','2018-02-17 19:10:42'),(31,9,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-17 19:16:13','2018-02-17 19:16:13'),(32,9,12,'true',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-17 19:16:13','2018-02-17 19:16:13'),(33,9,9,'23',1,'Cantidad de Personas','Personas',23,350.00,8050.00,'2018-02-17 19:16:13','2018-02-17 19:16:13'),(34,9,10,'3',2,'Cantidad de Noches','Noches',3,0.00,0.00,'2018-02-17 19:16:13','2018-02-17 19:16:13'),(35,9,14,'14',3,'Lugar Abierto Para Camping','Plan de Campamento',1,2702.00,2702.00,'2018-02-17 19:16:13','2018-02-17 19:16:13'),(36,9,17,'17',4,'Desayuno y Cena','Plan de Comida',1,2500.00,2500.00,'2018-02-17 19:16:13','2018-02-17 19:16:13'),(37,10,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-17 19:17:45','2018-02-17 19:17:45'),(38,10,12,'true',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-17 19:17:45','2018-02-17 19:17:45'),(39,10,9,'23',1,'Cantidad de Personas','Personas',23,350.00,8050.00,'2018-02-17 19:17:45','2018-02-17 19:17:45'),(40,10,10,'3',2,'Cantidad de Noches','Noches',3,0.00,0.00,'2018-02-17 19:17:45','2018-02-17 19:17:45'),(41,10,14,'14',3,'Lugar Abierto Para Camping','Plan de Campamento',1,2702.00,2702.00,'2018-02-17 19:17:45','2018-02-17 19:17:45'),(42,10,18,'18',4,'Desayuno, Cena y Comida','Plan de Comida',1,3500.00,3500.00,'2018-02-17 19:17:45','2018-02-17 19:17:45'),(43,11,11,'true',NULL,'Pisina','Pisina',1,2000.00,2000.00,'2018-02-17 19:21:37','2018-02-17 19:21:37'),(44,11,12,'true',NULL,'Kareokee','Kareokee',1,2800.00,2800.00,'2018-02-17 19:21:37','2018-02-17 19:21:37'),(45,11,9,'58',1,'Cantidad de Personas','Personas',58,350.00,20300.00,'2018-02-17 19:21:37','2018-02-17 19:21:37'),(46,11,10,'9',2,'Cantidad de Noches','Noches',9,0.00,0.00,'2018-02-17 19:21:37','2018-02-17 19:21:37'),(47,11,14,'14',3,'Lugar Abierto Para Camping','Plan de Campamento',1,2702.00,2702.00,'2018-02-17 19:21:37','2018-02-17 19:21:37'),(48,11,17,'17',4,'Desayuno y Cena','Plan de Comida',1,2500.00,2500.00,'2018-02-17 19:21:37','2018-02-17 19:21:37');
/*!40000 ALTER TABLE `QuotationsLines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (51,'2014_10_12_000000_create_users_table',1),(52,'2014_10_12_100000_create_password_resets_table',1),(53,'2017_12_19_100000_create_groups_table',1),(54,'2017_12_19_100001_create_availableoptions_table',1),(55,'2017_12_19_100002_create_quotations_table',1),(56,'2017_12_19_100003_create_quotationslines_table',1),(57,'2017_04_28_201013_create_acl_tables',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,NULL,NULL),(2,1,NULL,NULL),(3,1,NULL,NULL),(4,1,NULL,NULL),(4,2,NULL,NULL);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'admin-admin','Administrador de aplicacion','Administrador de la aplicacion','2018-02-17 13:16:28','2018-02-17 13:16:28'),(2,'ver-usuarios','Ver todos los usuarios','Ver todos los usuarios','2018-02-17 13:16:28','2018-02-17 13:16:28'),(3,'ver-roles','Ver todos los roles','Ver todos los roles','2018-02-17 13:16:28','2018-02-17 13:16:28'),(4,'admin-opcion','Administrar Opciones','Administrar Opciones','2018-02-17 13:16:28','2018-02-17 13:16:28');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,'2018-02-17 13:16:28',NULL);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrador','Administrador de aplicacion','2018-02-17 13:16:28','2018-02-17 13:16:28'),(2,'normal-user','Usuario Normal','Usuario que puede usar la administracion de opciones','2018-02-25 03:56:01','2018-02-25 03:56:01');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@admin.me','$2y$10$5tZfswkagckT6SLgvFDrMuLUT9/RXWhWWddcW8bv6tw7AkmYdd.UW',1,'RSnM46Ov4Hjcutoxm7ra6xYpfDNKHv5watlIHgAldYBvfGkQggXjSv0t8Xod','2018-02-17 13:16:28','2018-02-17 13:16:28');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dbCotVac'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-25  9:02:18
