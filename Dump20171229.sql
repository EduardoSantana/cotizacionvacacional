-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbCotVac
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AvailableOptions`
--

DROP TABLE IF EXISTS `AvailableOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AvailableOptions` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GroupsId` int(11) DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PersonsFrom` int(11) NOT NULL,
  `PersonsTo` int(11) NOT NULL,
  `NightsFrom` int(11) NOT NULL,
  `NightsTo` int(11) NOT NULL,
  `Price` decimal(16,2) NOT NULL,
  `OrderRank` int(11) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AvailableOptions`
--

LOCK TABLES `AvailableOptions` WRITE;
/*!40000 ALTER TABLE `AvailableOptions` DISABLE KEYS */;
INSERT INTO `AvailableOptions` VALUES (1,1,'Cantidad de Personas',NULL,'int',1,9,1,1,475.00,50,1,NULL,NULL),(2,1,'Cantidad de Personas',NULL,'int',10,14,1,1,425.00,50,1,NULL,NULL),(3,1,'Cantidad de Personas',NULL,'int',15,999,1,1,375.00,50,1,NULL,NULL),(4,1,'Cantidad de Personas',NULL,'int',1,9,2,2,450.00,50,1,NULL,NULL),(5,1,'Cantidad de Personas',NULL,'int',10,14,2,2,400.00,50,1,NULL,NULL),(6,1,'Cantidad de Personas',NULL,'int',15,999,2,2,350.00,50,1,NULL,NULL),(7,1,'Cantidad de Personas',NULL,'int',1,9,3,99,450.00,50,1,NULL,NULL),(8,1,'Cantidad de Personas',NULL,'int',10,14,3,99,400.00,50,1,NULL,NULL),(9,1,'Cantidad de Personas',NULL,'int',15,999,3,99,350.00,50,1,NULL,NULL),(10,2,'Cantidad de Noches',NULL,'int',0,9999,0,9999,0.00,60,1,NULL,NULL),(11,NULL,'Pisina',NULL,'bool',0,9999,0,9999,2000.00,300,1,NULL,NULL),(12,NULL,'Kareokee',NULL,'bool',0,9999,0,9999,2800.00,310,1,NULL,NULL),(13,3,'Apartamento Alquilado',NULL,'select',0,9999,0,9999,4382.00,632,1,NULL,NULL),(14,3,'Lugar Abierto Para Camping',NULL,'select',0,9999,0,9999,3519.00,519,1,NULL,NULL),(15,4,'Sin Comida',NULL,'select',0,9999,0,9999,0.00,400,1,NULL,NULL),(16,4,'Renta de Cosina',NULL,'select',0,9999,0,9999,1500.00,410,1,NULL,NULL),(17,4,'Desayuno y Cena',NULL,'select',0,9999,0,9999,2500.00,420,1,NULL,NULL),(18,4,'Desayuno, Cena y Comida',NULL,'select',0,9999,0,9999,3500.00,430,1,NULL,NULL),(19,4,'Desayuno, Cena y Comida',NULL,'select',0,9999,0,9999,4200.00,440,1,NULL,NULL);
/*!40000 ALTER TABLE `AvailableOptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `groups_name_unique` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES (1,'Personas',NULL,1,NULL,NULL),(2,'Noches',NULL,1,NULL,NULL),(3,'Plan de Campamento',NULL,1,NULL,NULL),(4,'Plan de Comida',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Quotations`
--

DROP TABLE IF EXISTS `Quotations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Quotations` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Celular` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Confirmed` tinyint(1) DEFAULT NULL,
  `TaxesPorc` decimal(4,2) NOT NULL,
  `Taxes` decimal(16,2) NOT NULL,
  `SubTotal` decimal(16,2) NOT NULL,
  `Total` decimal(16,2) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Quotations`
--

LOCK TABLES `Quotations` WRITE;
/*!40000 ALTER TABLE `Quotations` DISABLE KEYS */;
INSERT INTO `Quotations` VALUES (1,'asdf','asdf',NULL,'asdf',NULL,NULL,0.18,2703.42,15019.00,17722.42,1,'2017-12-29 16:13:41','2017-12-29 16:13:41'),(2,'eduardo','santana',NULL,'8093157934','me@me.com',NULL,0.18,3279.42,18219.00,21498.42,1,'2017-12-29 18:50:08','2017-12-29 18:50:08'),(3,'asdfasdf','asdfasdf',NULL,'asdfasdf',NULL,NULL,0.18,3279.42,18219.00,21498.42,1,'2017-12-29 19:07:07','2017-12-29 19:07:07'),(4,'asdf','asdf',NULL,'asdf',NULL,NULL,0.18,3279.42,18219.00,21498.42,1,'2017-12-29 19:12:19','2017-12-29 19:12:19'),(5,'Eduardo','Santana',NULL,'8093157934','me@me.com',NULL,0.18,3639.42,20219.00,23858.42,1,'2017-12-29 19:13:45','2017-12-29 19:13:45');
/*!40000 ALTER TABLE `Quotations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QuotationsLines`
--

DROP TABLE IF EXISTS `QuotationsLines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QuotationsLines` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `QuotationsId` int(11) NOT NULL,
  `AvailableOptionsId` int(11) NOT NULL,
  `SelectedValue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GroupsId` int(11) DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Qty` int(11) NOT NULL,
  `Price` decimal(16,2) NOT NULL,
  `Total` decimal(16,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QuotationsLines`
--

LOCK TABLES `QuotationsLines` WRITE;
/*!40000 ALTER TABLE `QuotationsLines` DISABLE KEYS */;
/*!40000 ALTER TABLE `QuotationsLines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_name_unique` (`name`),
  UNIQUE KEY `employees_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (44,'2014_10_12_000000_create_users_table',1),(45,'2014_10_12_100000_create_password_resets_table',1),(46,'2017_12_18_185714_create_employees_table',1),(47,'2017_12_19_100000_create_groups_table',1),(48,'2017_12_19_100001_create_availableoptions_table',1),(49,'2017_12_19_100002_create_quotations_table',1),(50,'2017_12_19_100003_create_quotationslines_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-29 15:48:37
