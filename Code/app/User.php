<?php

namespace App;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{

	use Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'active'
	];
		
	protected $casts = [
		'active' => 'integer'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	 /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

	public function roles()
	{
		return $this->belongsToMany('App\Role','role_user','user_id','role_id');
	}

	public function hasAnyRole($roles)
	{
		if(is_array($roles))
		{
			foreach ($roles as $role)
			{
				if($this->hasRole($roles))
				{
					return true;
				}
			}
		}
		else
		{
			if($this->hasRole($roles))
			{
				return true;
			}
		}
		return false;
	}

	public function hasRole($role)
	{
		$retVal = DB::select('select 1
						  from roles
						  inner join role_user on roles.id = role_user.role_id
						  where role_user.user_id = ? and roles.name = ? order by id limit 1', [$this->id , $role]);
		
		if(count($retVal))
		{
			return true;
		}
		return false;
	}

	public function hasAnyPerm($perms)
	{
		if(is_array($perms))
		{
			foreach ($perms as $perm)
			{
				if($this->hasPerm($perm))
				{
					return true;
				}
			}
		}
		else
		{
			if($this->hasPerm($perms))
			{
				return true;
			}
		}
		return false;
	}

	public function hasPerm($perm)
	{
		$retVal = DB::select('select 1
						  from role_user ru
						  inner join permission_role pr on ru.role_id = pr.role_id
						  inner join permissions pe on pr.permission_id = pe.id
						  where ru.user_id = ? and pe.name = ? order by id limit 1', [$this->id , $perm]);
		if(count($retVal))
		{
			return true;
		}
		return false;
	}
	

}
