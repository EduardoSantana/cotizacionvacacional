<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
	protected $table = 'Groups';

	protected $fillable = array(
		'Name',
		'Description',
		'Active'
	);
	
	protected $casts = [
		'Active' => 'integer'
	];

}
