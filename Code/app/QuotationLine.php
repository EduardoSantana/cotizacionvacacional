<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationLine extends Model
{
	protected $table = 'QuotationsLines';

	protected $fillable = array(
		'QuotationsId',
		'AvailableOptionsId',
		'SelectedValue',
		'GroupsId',
		'Name',
		'GroupName',
		'Qty',
		'Price',
		'Total'
	);

	protected $casts = [
		'Price' => 'float',
		'Qty' => 'integer',
		'GroupsId' => 'integer',
		'Total' => 'float',
		'AvailableOptionsId' => 'integer',
	];

}