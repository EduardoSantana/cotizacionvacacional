<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailableOptions extends Model
{
	protected $appends = ['GroupName'];

	protected $table = 'AvailableOptions';

	protected $fillable = array (
		'GroupsId',
		'Name',
		'Description',
		'Type',
		'PersonsFrom',
		'PersonsTo',
		'NightsFrom',
		'NightsTo',
		'Price',
		'OrderRank',
		'Active'
	);

	protected $casts = [
		'GroupsId' => 'integer',
		'Name' => 'string',
		'Description' => 'string',
		'Type' => 'string',
		'PersonsFrom' => 'integer',
		'PersonsTo' => 'integer',
		'NightsFrom' => 'integer',
		'NightsTo' => 'integer',
		'Price' => 'float',
		'OrderRank' => 'integer',
		'Active' => 'integer'
	];

	public function Groups()
	{
		return $this -> belongsTo('App\Groups','GroupsId', 'id');
	}

	public function getGroupNameAttribute()
	{
		return ($this -> GroupsId != null ? $this -> Groups -> Name : $this -> Name);
	}
}
