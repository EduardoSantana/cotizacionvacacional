<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{

	protected $table = 'Quotations';

	protected $fillable = array(
		'Name',
		'Surname',
		'Phone',
		'Celular',
		'Email',
		'Confirmed',
		'TaxesPorc',
		'Taxes',
		'SubTotal',
		'Total',
		'Active'
	);
		
	protected $casts = [
		'Active' => 'integer',
		'TaxesPorc' => 'float',
		'Taxes' => 'float',
		'SubTotal' => 'float',
		'Total' => 'float'
	];
	
}