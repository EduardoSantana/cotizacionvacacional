<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		'App\Model' => 'App\Policies\ModelPolicy',
	];

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerPolicies();

		Gate::define('roles', function ($user, $roles) {

			if(env('NOT_ROLES_AND_PERMS', false)) { return true; }
			return $user->hasAnyRole($roles);

			/* On Controller
			 * if(Gate::denies('roles', ['role-name'])) {
			 * 		return response()->view('errors.401', [], 401);
			 * }
			 * if(!Gate::allows('roles', ['role-name'])) {
			 * 		return response()->view('errors.401', [], 401);
			 * }
			 * And Also
			 * $this->authorize('roles', ['role-name']);
			 * And Also
			 * if(Auth::user()->can('roles', ['role-name'])) {
			 * 		return response()->view('errors.401', [], 401);
			 * }
			 * if(Auth::user()->cannot('roles', ['role-name'])) {
			 * 		return response()->view('errors.401', [], 401);
			 * }
			 * And Also on Blade Templates
			 * @can('roles', ['role-name']) ..... @endcan
			 * @cannot('roles', ['role-name']) ..... @endcannot
			 */

		});

		Gate::define('perms', function ($user, $perms) {
			if(env('NOT_ROLES_AND_PERMS', false)) { return true; }
			return $user->hasAnyPerm($perms);
		});
	
		//
	}
}
