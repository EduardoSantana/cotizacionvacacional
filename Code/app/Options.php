<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
	protected $appends = ['GroupName', 'Persons', 'Nights', 'ActiveName'];

	protected $table = 'AvailableOptions';

	protected $fillable = array (
		'GroupsId',
		'Name',
		'Description',
		'Type',
		'PersonsFrom',
		'PersonsTo',
		'NightsFrom',
		'NightsTo',
		'Price',
		'OrderRank',
		'Active'
	);

	protected $casts = [
		'GroupsId' => 'integer',
		'Name' => 'string',
		'Description' => 'string',
		'Type' => 'string',
		'PersonsFrom' => 'integer',
		'PersonsTo' => 'integer',
		'NightsFrom' => 'integer',
		'NightsTo' => 'integer',
		'Price' => 'float',
		'OrderRank' => 'integer',
		'Active' => 'integer'
	];

	public function Groups()
	{
		return $this->belongsTo('App\Groups','GroupsId', 'Id');
	}

	public function getGroupNameAttribute()
	{
		return ($this -> GroupsId != null ? $this -> Groups -> Name : $this -> Name);
	}

	public function getPersonsAttribute()
	{
		return $this -> PersonsFrom . " - " . $this -> PersonsTo;
	}

	public function getNightsAttribute()
	{
		return $this -> NightsFrom . " - " . $this -> NightsTo;
	}

	public function getActiveNameAttribute()
	{
		return ($this -> Active ? "Si" : "No");
	}

}
