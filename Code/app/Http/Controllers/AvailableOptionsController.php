<?php

namespace App\Http\Controllers;

use App\QuotationLine;
use App\Quotation;
use App\AvailableOptions;
use Illuminate\Http\Request;
use Mail;
use DB;

class AvailableOptionsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null, Request $request) 
	{
		if ($id == null) 
		{
			$persons = 15;
			$nights = 3;

			if ($request -> has('persons') && $request -> input('persons') > 0) 
			{
				$persons = $request -> input('persons');
			}
			else
			{
				$persons = 15;
			}

			if ($request -> has('nights') && $request -> input('nights') > 0) 
			{
				$nights = $request -> input('nights');
			}
			else
			{
				$nights = 3;
			}
			
			return AvailableOptions::whereRaw($persons . ' between PersonsFrom and PersonsTo') 
								-> whereRaw($nights . ' between NightsFrom and NightsTo')
								-> whereRaw(' Active = 1 ')
								-> orderBy('GroupsId', 'asc')
								-> get();
		} 
		else 
		{
			return $this -> show($id);
		}
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request) 
	{
		$quotation = new Quotation();
		$quotation -> Name = $request -> input('Name');
		$quotation -> Surname = $request -> input('Surname');
		$quotation -> Phone = $request -> input('Phone');
		$quotation -> Celular = $request -> input('Celular');
		$quotation -> Email = $request -> input('Email');
		$quotation -> Confirmed = $request -> input('Confirmed');
		$quotation -> TaxesPorc = 0.18;
		$quotation -> SubTotal = $request -> input('total');
		$quotation -> Taxes = ($quotation -> TaxesPorc * $quotation -> SubTotal);
		$quotation -> Total = ($quotation -> Taxes + $quotation -> SubTotal);
		$quotation -> Active = 1;
		$quotation -> save();

		foreach ($request -> input('items', []) as $line) 
		{
			$quotationLine = new QuotationLine();
			$quotationLine -> QuotationsId = $quotation -> id;
			$quotationLine -> AvailableOptionsId = $line['id'];
			$quotationLine -> SelectedValue = $line['selected'];
			$quotationLine -> GroupsId = (array_key_exists('GroupsId', $line) == true ? $line['GroupsId'] : null);
			$quotationLine -> Name = $line['Name'];
			$quotationLine -> GroupName = $line['GroupName'];
			$quotationLine -> Qty = ($line['Type'] == 'int' ? $line['selected'] : 1);
			$quotationLine -> Price =  $line['Price'];
			$quotationLine -> Total = ($quotationLine -> Qty * $quotationLine -> Price);
			$quotationLine -> save();
		}

		if ($request -> input('sendEmail', 0) == 1) {
			$this -> sendEmail($quotation);
		}

		if ($request -> input('sendEmailAdmin', 0) == 1) {
			$this -> sendEmailAdmin($quotation);
		}

		return 'Quotation record successfully created with id ' . $quotation -> id;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		return AvailableOptions::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request  $request
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id) {
		return "Sucess updating user #";
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		return "AvailableOptions record successfully deleted #";
	}

	/**
	 * Send an Email with the specified Quotation from memory
	 * vacacionalpinarquemado
	 * @param Quotation object
	 * @return null
	 */
	protected function sendEmail($quotation)
	{
		$quotation -> emailAdmin = false;
		$quotation -> subject = 'Cotizacion - Vacacional Pinar Quemado, No: ' . $quotation -> id;
		$quotation -> titulo1 = 'Informacion de Cotizacion, No: ' . $quotation -> id;
		$quotation -> texto1 = 'El monto de la cotizacicon es: ' . number_format($quotation -> SubTotal, 0) . ', mas ' . number_format($quotation -> Taxes, 0) . ' de impuestos. Para un total de ' . number_format($quotation -> Total, 0);
		$quotation -> FullName = $quotation -> Name . ' ' . $quotation -> Surame;
		$quotation -> items = DB::select("select line.*, opt.Type from QuotationsLines line inner join AvailableOptions opt on opt.id = line.AvailableOptionsId left join Groups gro on gro.id = opt.GroupsId where line.QuotationsId = :id", 
			['id' => $quotation -> id]);
		//Debugbar::info($quotation);
		Mail::send('emails.quotation', 
				  ['quotation' => $quotation], 
				 function ($m) use ($quotation) {
					 $m -> to($quotation->Email, $quotation->Name)
						-> subject($quotation -> subject);
				 });
	}
	
	/**
	 * Send an Email with the specified Quotation from memory to a admin
	 * vacacionalpinarquemado
	 * @param Quotation object
	 * @return null
	 */
	protected function sendEmailAdmin($quotation)
	{
		$mail_admin = env('MAIL_ADMIN','noadminmail@me.com');
		$mail_adminname = env('MAIL_ADMINNAME','noadminmailname');
		$quotation -> emailAdmin = true;
		$quotation -> subject = 'Cotizacion - Vacacional Pinar Quemado, No: ' . $quotation -> id;
		$quotation -> titulo1 = 'Informacion de Cotizacion recibida, No: ' . $quotation -> id;
		$quotation -> texto1 = 'Has recibido una solicitud de cotizacion, No: '  . $quotation -> id . ' de ' . $quotation -> Name . ' ' . $quotation -> Surname . ', la cual ha solicitado asistencia personalizada. ';
		$quotation -> texto1 = $quotation -> texto1 . 'El monto de la cotizacicon es: ' . number_format($quotation -> SubTotal, 0) . ', mas ' . number_format($quotation -> Taxes, 0) . ' de impuestos. Para un total de ' . number_format($quotation -> Total, 0);
		$quotation -> FullName = $mail_adminname;
		$quotation -> items = DB::select("select line.*, opt.Type from QuotationsLines line inner join AvailableOptions opt on opt.id = line.AvailableOptionsId left join Groups gro on gro.id = opt.GroupsId where line.QuotationsId = :id", 
			['id' => $quotation -> id]);
		//Debugbar::info($quotation);
		Mail::send('emails.quotation', 
				  ['quotation' => $quotation, 
				  'mail_admin' => $mail_admin, 
				  'mail_adminname' => $mail_adminname], 
				 function ($m) use ($quotation, $mail_admin, $mail_adminname) {
					 $m -> to($mail_admin, $mail_adminname)
						-> subject($quotation -> subject);
				 });
	}
}
