<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use JWTAuth;
use JWTAuthException;
use App\User;

class ApiController extends Controller
{

    public function __construct()
    {
        $this->user = new User;
    }
    
    public function login(Request $request)
	{
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'invalid_email_or_password',
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'failed_to_create_token',
            ]);
        }
        return response()->json([
            'response' => 'success',
            'token' => $token,
        ]);
    }

    public function getAuthUser(Request $request){
        /*
		$user = JWTAuth::toUser($request->token);        
        return response()->json(['result' => $user]);
		*/
		try 
		{
			if (! $user = JWTAuth::parseToken()->authenticate()) 
			{
				return response()->json(['user_not_found'], 404);
			}
		} 
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) 
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		} 
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) 
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		} 
		catch (Tymon\JWTAuth\Exceptions\JWTException $e) 
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}
		
		// the token is valid and we have found the user via the sub claim
		return response()->json(compact('user'));
    }

}