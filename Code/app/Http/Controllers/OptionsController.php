<?php

namespace App\Http\Controllers;

use App\Options;
use Illuminate\Http\Request;

class OptionsController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 public function __construct()
	 {
		$this->middleware('auth');
	 }
	 */
   

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null, Request $request) 
	{
		if ($id == null) 
		{
			$persons = 15;
			$nights = 3;

			if ($request -> has('persons') && $request -> input('persons') > 0) 
			{
				$persons = $request -> input('persons');
			}
			else
			{
				$persons = 15;
			}

			if ($request -> has('nights') && $request -> input('nights') > 0) 
			{
				$nights = $request -> input('nights');
			}
			else
			{
				$nights = 3;
			}
			
			return Options::whereRaw($persons . ' between PersonsFrom and PersonsTo') 
								-> whereRaw($nights . ' between NightsFrom and NightsTo')
								-> orderBy('GroupsId', 'asc')
								-> get();
		} 
		else 
		{
			return $this -> show($id);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request) {
		$options = new Options();
		$options -> GroupsId = $request->input('GroupsId');
		$options -> Name = $request->input('Name');
		$options -> Description = $request->input('Description');
		$options -> Type = $request->input('Type');
		$options -> PersonsFrom = $request->input('PersonsFrom');
		$options -> PersonsTo = $request->input('PersonsTo');
		$options -> NightsFrom = $request->input('NightsFrom');
		$options -> NightsTo = $request->input('NightsTo');
		$options -> Price = $request->input('Price');
		$options -> OrderRank = $request->input('OrderRank');
		$options -> Active = $request->input('Active');
		$options -> save();
		return 'Options record successfully created with id ' . $options -> id;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		return Options::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request  $request
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id) 
	{
		$options = Options::find($id);
		$options -> Name = $request -> input('Name');
		$options -> Type = $request -> input('Type');
		$options -> PersonsFrom = $request -> input('PersonsFrom');
		$options -> PersonsTo = $request -> input('PersonsTo');
		$options -> NightsFrom = $request -> input('NightsFrom');
		$options -> NightsTo = $request -> input('NightsTo');
		$options -> Price = $request -> input('Price');
		$options -> Active = $request -> input('Active');
		if ($request -> has('GroupsId')) {
			$options -> GroupsId = $request -> input('GroupsId');
		}
		if ($request -> has('Description')) {
			$options -> Description = $request -> input('Description');
		}
		if ($request -> has('OrderRank')) {
			$options -> OrderRank = $request -> input('OrderRank');
		}
		$options -> save();
		return "Sucess updating user # " . $options -> id;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$options = Options::find($id);
		$options -> delete();
		return "Options record successfully deleted # " . $id;
	}
}
