<?php

namespace App\Http\Controllers;

use App\Groups;
use Illuminate\Http\Request;

class GroupsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null, Request $request) {
        if ($id == null) 
        {
            return Groups::get();
        } 
        else 
        {
            return $this -> show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        $groups = new Groups();
        $groups -> Name = $request -> input('Name');
        if ($request -> has('Description')) {
            $groups -> Description = $request -> input('Description');
        }
        $groups -> Active = $request->input('Active');
        $groups -> save();
        return 'Groups record successfully created with id ' . $groups -> id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Groups::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) 
    {
        $groups = Groups::find($id);
        $groups -> Name = $request -> input('Name');
        if ($request -> has('Description')) {
            $groups -> Description = $request -> input('Description');
        }
        $groups -> Active = $request -> input('Active');
        $groups -> save();
        return "Sucess updating groups # " . $groups -> id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $groups = Groups::find($id);
        $groups -> delete();
        return "Groups record successfully deleted # " . $id;
    }
}
