<?php

namespace App\Http\Controllers;

use App\Quotation;
use App\QuotationLine;
use Illuminate\Http\Request;
use Mail;
use Debugbar;
use DB;

class QuotationsController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null, Request $request) 
	{
		if ($id == null) 
		{
			return Quotation::get();
		} 
		else 
		{
			return $this -> show($id);
		}
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request) 
	{
		return "";
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) 
	{
		$retVal = Quotation::find($id);
		if(!$retVal)
		{
			return null;
		}
		
		$retVal -> items = DB::select("select opt.*, ifnull(gro.Name, line.Name) as GroupName, line.SelectedValue as selected from QuotationsLines line inner join AvailableOptions opt on opt.id = line.AvailableOptionsId left join Groups gro on gro.id = opt.GroupsId where line.QuotationsId = :id", 
			['id' => $id]);
		return $retVal;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) 
	{
		//$quotation = Quotation::find($id);
		//$quotation -> delete();
		return "Quotation record successfully deleted # " . $id;
	}

	
	/**
	 * Send an Email with the specified Quotation  Id from storage.
	 *
	 * @param  int  $id
	 * @return null
	 */
	public function sendEmailJS($id)
	{
		$quotation = Quotation::findOrFail($id);
		$this -> sendEmailAdmin($quotation);
		$this -> sendEmail($quotation);
	}

	/**
	 * Send an Email with the specified Quotation from memory
	 * vacacionalpinarquemado
	 * @param Quotation object
	 * @return null
	 */
	protected function sendEmail($quotation)
	{
		$quotation -> emailAdmin = false;
		$quotation -> subject = 'Cotizacion - Vacacional Pinar Quemado, No: ' . $quotation -> id;
		$quotation -> titulo1 = 'Informacion de Cotizacion, No: ' . $quotation -> id;
		$quotation -> texto1 = 'El monto de la cotizacicon es: ' . number_format($quotation -> SubTotal, 0) . ', mas ' . number_format($quotation -> Taxes, 0) . ' de impuestos. Para un total de ' . number_format($quotation -> Total, 0);
		$quotation -> FullName = $quotation -> Name . ' ' . $quotation -> Surame;
		$quotation -> items = DB::select("select line.*, opt.Type  from QuotationsLines line inner join AvailableOptions opt on opt.id = line.AvailableOptionsId left join Groups gro on gro.id = opt.GroupsId where line.QuotationsId = :id", 
			['id' => $quotation -> id]);
		//Debugbar::info($quotation);
		Mail::send('emails.quotation', 
				  ['quotation' => $quotation], 
				 function ($m) use ($quotation) {
					 $m -> to($quotation->Email, $quotation->Name)
						-> subject($quotation -> subject);
				 });
	}
	
	/**
	 * Send an Email with the specified Quotation from memory to a admin
	 * vacacionalpinarquemado
	 * @param Quotation object
	 * @return null
	 */
	protected function sendEmailAdmin($quotation)
	{
		$mail_admin = env('MAIL_ADMIN','noadminmail@me.com');
		$mail_adminname = env('MAIL_ADMINNAME','noadminmailname');
		$quotation -> emailAdmin = true;
		$quotation -> subject = 'Cotizacion - Vacacional Pinar Quemado, No: ' . $quotation -> id;
		$quotation -> titulo1 = 'Informacion de Cotizacion recibida, No: ' . $quotation -> id;
		$quotation -> texto1 = 'Has recibido una solicitud de cotizacion, No: '  . $quotation -> id . ' de ' . $quotation -> Name . ' ' . $quotation -> Surname . ', la cual ha solicitado asistencia personalizada. ';
		$quotation -> texto1 = $quotation -> texto1 . 'El monto de la cotizacicon es: ' . number_format($quotation -> SubTotal, 0) . ', mas ' . number_format($quotation -> Taxes, 0) . ' de impuestos. Para un total de ' . number_format($quotation -> Total, 0);
		$quotation -> FullName = $mail_adminname;
		$quotation -> items = DB::select("select line.*, opt.Type from QuotationsLines line inner join AvailableOptions opt on opt.id = line.AvailableOptionsId left join Groups gro on gro.id = opt.GroupsId where line.QuotationsId = :id", 
			['id' => $quotation -> id]);
		//Debugbar::info($quotation);
		Mail::send('emails.quotation', 
				  ['quotation' => $quotation, 
				  'mail_admin' => $mail_admin, 
				  'mail_adminname' => $mail_adminname], 
				 function ($m) use ($quotation, $mail_admin, $mail_adminname) {
					 $m -> to($mail_admin, $mail_adminname)
						-> subject($quotation -> subject);
				 });
	}
}
