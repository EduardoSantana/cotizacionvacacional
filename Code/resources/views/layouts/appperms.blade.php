<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>{{ config('app.name', 'Laravel') }} | Autenticated User</title>

		<!-- Fonts -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">

		<!-- Styles -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		
	</head>
	<body id="app-layout">
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">

					<!-- Collapsed Hamburger -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!-- Branding Image -->
					<a class="navbar-brand" href="{{ url('/') }}">
						{{ config('app.name', 'Laravel') }}
					</a>
				</div>

				<div class="collapse navbar-collapse" id="app-navbar-collapse">
					<!-- Left Side Of Navbar -->
					@if (Auth::guest())
					<span></span>
					@else
					<ul class="nav navbar-nav">
						<li><a href="{{ url('/') }}">Home</a></li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								Administracion <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								@can('perms',['ver-usuarios'])
								<li><a href="{{ route('users.index') }}">Usuarios</a></li>
								<li role="separator" class="divider"></li>
								@endcan
								@can('perms',['ver-roles'])
									<li><a href="{{ route('roles.index') }}">Roles</a></li>
								@endcan
							</ul>
						</li>
				
					</ul>
					@endif
					<!-- Right Side Of Navbar -->
					<ul class="nav navbar-nav navbar-right">
						<!-- Authentication Links -->
						@if (Auth::guest())
						<li><a href="{{ url('/login') }}">Login</a></li>
						@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<i class="fa fa-user fa-fw"></i> {{ Auth::user()->name }} <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="{{ route('logout') }}"
										onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
									   <i class="fa fa-btn fa-sign-out"></i> Logout
									</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							</ul>

						</li>
						@endif
					</ul>
				</div>
			</div>
		</nav>

		@yield('content')

		<!-- JavaScripts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

		@yield('scripts')
		<script type="text/javascript">
			$(document).ready(function() {
				$('.navbar-collapse a[href="'+location.href+'"]').parents('li').addClass('active');
				$('[data-toggle="popover"]').popover();
			});
		</script>

		<!-- Modal -->
		<div class="modal fade" id="myModalPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title"></h3>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label for="exampleInputEmail1">Material 1</label>
								<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Material 1">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2">Material 2</label>
								<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Material 2">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail3">Material 3</label>
								<input type="text" class="form-control" id="exampleInputEmail3" placeholder="Material 3">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail4">Material 4</label>
								<input type="text" class="form-control" id="exampleInputEmail4" placeholder="Material 4">
							</div>
						</form>
					</div>
					<!--<div class="modal-footer">-->
					<!--    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
					<!--    <button type="button" class="btn btn-primary">Save changes</button>-->
					<!--</div>-->
				</div>
			</div>
		</div>
	</body>
</html>
