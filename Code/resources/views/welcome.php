<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
  <meta charset="UTF-8"></meta>
  <meta name="viewport" content="width=device-width, initial-scale=1"></meta>

	<title>App - Cotizador</title>
	<!-- STYLES -->
	<!-- build:css lib/css/main.min.css -->
	<link rel="stylesheet" type="text/css" href="components/bootstrap/dist/css/bootstrap.min.css"></link>
	<link rel="stylesheet" type="text/css" href="components/font-awesome/css/font-awesome.min.css"></link>
	<link rel="stylesheet" type="text/css" href="components/rdash-ui/dist/css/rdash.min.css"></link>
	<!-- endbuild -->
	<!-- SCRIPTS -->
	<!-- build:js lib/js/main.min.js -->
	<script type="text/javascript" src="components/angular/angular.min.js"></script>
	<script type="text/javascript" src="components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	<script type="text/javascript" src="components/angular-cookies/angular-cookies.min.js"></script>
	<script type="text/javascript" src="components/angular-ui-router/release/angular-ui-router.min.js"></script>
	<script type="text/javascript" src="components/satellizer/dist/satellizer.min.js"></script>
	<!-- endbuild -->
	<!-- Custom Scripts -->

	<script type="text/javascript" src="js/module.js"></script>
	<script type="text/javascript" src="js/routes.js"></script>
	<script type="text/javascript" src="js/controllers/alert-ctrl.js"></script>
	<script type="text/javascript" src="js/controllers/master-ctrl.js"></script>
	<script type="text/javascript" src="js/controllers/quotation-ctrl.js"></script>
	<script type="text/javascript" src="js/controllers/quotation-modal-ctrl.js"></script>
	<script type="text/javascript" src="js/directives/loading.js"></script>
	<script type="text/javascript" src="js/directives/widget.js"></script>
	<script type="text/javascript" src="js/directives/widget-body.js"></script>
	<script type="text/javascript" src="js/directives/widget-footer.js"></script>
	<script type="text/javascript" src="js/directives/widget-header.js"></script>
   
</head>
<body ng-controller="MasterCtrl">
	<div id="page-wrapper" ng-class="{'open': toggle}" ng-cloak>
		<div id="content-wrapper">
			<div class="page-content">

				<!-- Header Bar -->
				<div class="row header">
					<div class="col-xs-12">
						<div class="user pull-right">
						
						</div>
						<div class="meta">
							<div class="page">
								Cotizador
							</div>
						</div>
					</div>
				</div>
				<!-- End Header Bar -->

				<!-- Main Content -->
				<div ui-view></div>

			</div><!-- End Page Content -->
		</div><!-- End Content Wrapper -->
	</div><!-- End Page Wrapper -->
</body>
</html>
