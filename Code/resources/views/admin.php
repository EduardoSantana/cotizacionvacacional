<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
  <meta charset="UTF-8"></meta>
  <meta name="viewport" content="width=device-width, initial-scale=1"></meta>

	<title>App - Cotizador</title>
	<!-- STYLES -->
	<!-- build:css lib/css/main.min.css -->
    <link rel="stylesheet" type="text/css" href="components/bootstrap/dist/css/bootstrap.min.css"></link>
	<link rel="stylesheet" type="text/css" href="components/font-awesome/css/font-awesome.min.css"></link>
	<link rel="stylesheet" type="text/css" href="components/rdash-ui/dist/css/rdash-admin.min.css"></link>
	<!-- endbuild -->
	<!-- SCRIPTS -->
	<!-- build:js lib/js/main.min.js -->
	<script type="text/javascript" src="components/angular/angular.min.js"></script>
	<script type="text/javascript" src="components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	<script type="text/javascript" src="components/angular-cookies/angular-cookies.min.js"></script>
	<script type="text/javascript" src="components/angular-ui-router/release/angular-ui-router.min.js"></script>
	<script type="text/javascript" src="components/satellizer/dist/satellizer.js"></script>
	<!-- endbuild -->
	<!-- Custom Scripts -->
	<script type="text/javascript" src="js-admin/module.js"></script>
	<script type="text/javascript" src="js-admin/routes.js"></script>
	<script type="text/javascript" src="js-admin/controllers/alert-ctrl.js"></script>
	<script type="text/javascript" src="js-admin/controllers/master-ctrl.js"></script>
	<script type="text/javascript" src="js-admin/controllers/quotations-ctrl.js"></script>
	<script type="text/javascript" src="js-admin/controllers/options-ctrl.js"></script>
	<script type="text/javascript" src="js-admin/controllers/options-modal-ctrl.js"></script>
	<script type="text/javascript" src="js/controllers/quotation-ctrl.js"></script>
	<script type="text/javascript" src="js/controllers/quotation-modal-ctrl.js"></script>
	<script type="text/javascript" src="js/directives/loading.js"></script>
	<script type="text/javascript" src="js/directives/widget.js"></script>
	<script type="text/javascript" src="js/directives/widget-body.js"></script>
	<script type="text/javascript" src="js/directives/widget-footer.js"></script>
	<script type="text/javascript" src="js/directives/widget-header.js"></script>

</head>
<body ng-controller="MasterCtrl">
	<div id="page-wrapper" ng-class="{'open': toggle}" ng-cloak>

		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<ul class="sidebar">
				<li class="sidebar-main">
					<a ng-click="toggleSidebar()">
						Cotizador 
						<span class="menu-icon glyphicon glyphicon-transfer"></span>
					</a>
				</li>
				<li class="sidebar-title"><span>NAVIGATION</span></li>
				<li class="sidebar-list">
					<a href="#/quotations">Cotizaciones <span class="menu-icon fa fa-money"></span></a>
				</li>
				<li class="sidebar-list">
					<a href="#/options">Opciones <span class="menu-icon fa fa-filter"></span></a>
				</li>
				<li class="sidebar-list">
					<a href="#" ng-click="logout()"> 
						Logout
						<span class="menu-icon fa fa-user-secret"></span>
                    </a> 
					<form id="logout-form" 
						action="/logout" 
						method="POST" 
						style="display: none;">
					</form>
				</li>
			</ul>
			<div class="sidebar-footer">
				<div class="col-xs-4">
					<!-- <a href="#">
						Support
					</a> -->
				</div>
				<div class="col-xs-4">
				<!--  <a href="#">
						Support
					</a> -->
				</div>
				<div class="col-xs-4">
				 <!--  <a href="#">
						Support
					</a> -->
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div id="content-wrapper">
			<div class="page-content">

				<!-- Header Bar -->
				<div class="row header">
					<div class="col-xs-12">
						<div class="user pull-right">
							<div class="item dropdown" uib-dropdown>
								<a href="#" class="dropdown-toggle" uib-dropdown-toggle>
									<span> {{ currentUser.name }} </span>
									<img src="img/avatar.jpg">
								</a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li class="dropdown-header">
										{{ currentUser.name }}
									</li>
									<li class="divider"></li>
									<li class="link">
										<a href="#" ng-click="logout()"> 
											Logout
										</a> 
									</li>
								</ul>
							</div>
						</div>
						<div class="meta">
							<div class="page">
								Administrador de opciones
							</div>
						</div>
					</div>
				</div>
				<!-- End Header Bar -->

				<!-- Main Content -->
				<div ui-view></div>

			</div><!-- End Page Content -->
		</div><!-- End Content Wrapper -->
	</div><!-- End Page Wrapper -->
</body>
</html>
