angular.module('app')
	.controller('OptionsCtrl', 
		['$scope', 
		 '$cookieStore', 
		 '$http', 
		 'API_URL', 
		 '$httpParamSerializerJQLike', 
		 '$uibModal',
		 '$timeout',
		 OptionsCtrl
		 ]);

function OptionsCtrl($scope, 
					   $cookieStore, 
					   $http, 
					   API_URL,
					   $httpParamSerializerJQLike,
					   $uibModal,
					   $timeout) {

    $scope.buscar = {};
    $scope.buscar.txtPersonas = "15";
    $scope.buscar.txtNoches = "3";

	$http.get(API_URL + "options")
				.success(function(response) {
					$scope.options = response;
				});

    $scope.changeTxtControl = function changeTxtControl() {
        $timeout(function () {
            $http.get(API_URL + "options?persons=" + $scope.buscar.txtPersonas + "&nights=" + $scope.buscar.txtNoches)
                .success(function (response) {
                    $scope.options = response;
                });
        }, 500);
    }

	// show modal form
	$scope.toggle = function(modalstate, id) {
	   
		var dialog = $uibModal.open({
			templateUrl: "myModalContent.html",
			controller: "OptionsModalCtrl",
			controllerAs: "$ctrl",
			resolve: {
				modalstate: function () { return modalstate; },
				id: id
			}
		});

		dialog.result.then(function () {
			$timeout(function () {
                $http.get(API_URL + "options?persons=" + $scope.buscar.txtPersonas + "&nights=" + $scope.buscar.txtNoches)
				.success(function(response) {
					$scope.options = response;
				});
			}, 500);
		}, function () {
			 //console.log('Modal 2 dismissed at: ' + new Date());
		});

	}

	// delete record
	$scope.confirmDelete = function(id) {
		var isConfirmDelete = confirm('Are you sure you want this record?');
		if (isConfirmDelete) {
			$http({
				method: 'POST',
				url: API_URL + 'options/' + id + '/delete'
			}).
			success(function(data) {
				$timeout(function () {
					 $http.get(API_URL + "options")
					.success(function(response) {
						$scope.options = response;
					});
				}, 500);
			}).
			error(function(data) {
				console.log(data);
				alert('Unable to delete');
			});
		} else {
			return false;
		}
	}
}