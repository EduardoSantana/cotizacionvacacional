angular.module('app')
    .controller('QuotationsCtrl',
    ['$scope',
        '$cookieStore',
        '$http',
        'API_URL',
        '$httpParamSerializerJQLike',
        '$uibModal',
        '$timeout',
        '$auth',
        '$rootScope',
        QuotationsCtrl
    ]);

function QuotationsCtrl($scope,
    $cookieStore,
    $http,
    API_URL,
    $httpParamSerializerJQLike,
    $uibModal,
    $timeout,
    $auth,
    $rootScope) {

    if (localStorage.email && localStorage.password) {

        var credentials = {
            email: localStorage.email,
            password: localStorage.password
        };

        // Use Satellizer's $auth service to login
        $auth.login(credentials).then(function () {
            
            // Return an $http request for the now authenticated
            // user so that we can flatten the promise chain
            return $http.get('api/auth/user');

        // Handle errors
        }, function (error) {

            var asdfasdf = error.data.error;

        // Because we returned the $http.get request in the $auth.login
        // promise, we can chain the next promise to the end here
        }).then(function (response) {

            // Stringify the returned data to prepare it
            // to go into local storage
            var user = JSON.stringify(response.data.user);

            // Set the stringified user data into local storage
            localStorage.setItem('user', user);

            // The user's authenticated state gets flipped to
            // true so we can now show parts of the UI that rely
            // on the user being logged in
            $rootScope.authenticated = true;

            // Putting the user's data on $rootScope allows
            // us to access it anywhere across the app
            $rootScope.currentUser = response.data.user;

            // Everything worked out so we can now redirect to
            // the users state to view the data
            // $state.go('users');
            initController();
            localStorage.removeItem("email");
            localStorage.removeItem("password");

        });
    
    } else {
        initController();
    }

    // delete record
    $scope.confirmDelete = function (id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'POST',
                url: API_URL + 'quotations/' + id + '/delete'
            }).
                success(function (data) {
                    $timeout(function () {
                        $http.get(API_URL + "quotations")
                            .success(function (response) {
                                $scope.quotations = response;
                            });
                    }, 500);
                }).
                error(function (data) {
                    console.log(data);
                    alert('Unable to delete');
                });
        } else {
            return false;
        }
    }

    // send email js from a record
    $scope.sendEmailJS = function (id) {
        var isConfirm = confirm('Are you sure you want to send an email from this record?');
        if (isConfirm) {
            $http({
                method: 'POST',
                url: API_URL + 'quotations/' + id + '/sendEmailJS'
            }).
                success(function (data) {
                }).
                error(function (data) {
                    console.log(data);
                    alert('Unable to send mail');
                });
        } else {
            return false;
        }
    }

    function initController() {
        $http.get(API_URL + "quotations")
            .success(function (response) {
                $scope.quotations = response;
            });
    }

    $rootScope.logout = function logout() {

        $auth.logout().then(function () {

            // Remove the authenticated user from local storage
            localStorage.removeItem('user');
            //localStorage.removeItem('satellizer_token');

            // Flip authenticated to false so that we no longer
            // show UI elements dependant on the user being logged in
            $rootScope.authenticated = false;

            // Remove the current user info from rootscope
            $rootScope.currentUser = null;

            // onclick = "event.preventDefault(); 
            document.getElementById('logout-form').submit();

        });

    }

}