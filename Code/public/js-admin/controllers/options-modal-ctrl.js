/**
 * Options Modal Controller
 */
angular.module('app')
	.controller('OptionsModalCtrl', 
		['$scope', 
		 '$cookieStore', 
		 '$http', 
		 '$uibModalInstance',
		 'modalstate',
		 'id',
		 'API_URL',
		 '$httpParamSerializerJQLike',
		 OptionsModalCtrl
		 ]);

function OptionsModalCtrl($scope, $cookieStore, $http, $uibModalInstance, modalstate, id, API_URL, $httpParamSerializerJQLike) 
{
	var $ctrl = this;
	$ctrl.form_title = "";
	$ctrl.form_action = "";
	$ctrl.modalstate = modalstate;
	$scope.option = {};
	$scope.groups = [];

	switch ($ctrl.modalstate) {
		case 'delete':
			$ctrl.form_title = 'Eliminar Opcion?';
			$ctrl.form_action = "Eliminar";
			break;
		case 'add':
			$ctrl.form_title = 'Agregar Nueva Opcion';
			$ctrl.form_action = "Agregar";
			$http.get(API_URL + 'groups')
			.success(function(response) {
				//console.log(response);
				$scope.groups = response;
			});
			break;
		case 'edit':
			$ctrl.form_title = 'Detalle  de Opcion';
			$ctrl.form_action = "Actualizar";
			$ctrl.id = id;
			$http.get(API_URL + 'groups')
			.success(function(response) {
				//console.log(response);
				$scope.groups = response;
			});
			$http.get(API_URL + 'options/' + id)
			.success(function(response) {
				//console.log(response);
				$scope.option = response;
			});
			break;
		default:
			break;
	}

	$ctrl.ok = function () {
		if ($ctrl.modalstate == 'delete') {	
			deleteExisting();
		}
		else{
			saveNewOrExisting($ctrl.modalstate, id);
		}
	};

	$ctrl.cancel = function () {
		$uibModalInstance.dismiss();
	};

	//save new record / update existing record
	var saveNewOrExisting = function(modalstate2, id2) {
		var url = API_URL + "options";

		//append option id to the URL if the form is in edit mode
		if (modalstate2 === 'edit'){
			url += "/" + id2;
		}
		var myData = $httpParamSerializerJQLike($scope.option);
		$http({
			method: 'POST',
			url: url,
			data: myData,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(response) {
			//console.log(response);
			$uibModalInstance.close();
			//location.reload();
		}).error(function(response) {
			//console.log(response);
			$uibModalInstance.dismiss();
			//alert('This is embarassing. An error has occured. Please check the log for details');
		});
	}

	var deleteExisting = function(){
		$http({
			method: 'POST',
			url: API_URL + 'options/' + id + '/delete'
		}).
		success(function(data) {
			$uibModalInstance.close();
		}).
		error(function(data) {
			console.log(data);
			$uibModalInstance.dismiss();
		});
	}

}
