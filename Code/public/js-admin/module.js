angular.module('app', ['ui.bootstrap', 'ui.router', 'ngCookies', 'satellizer'])
    .constant('API_URL', 'http://cotizacionvacacional.test/api/v1/')
	.constant('PERSONS_GROUP_ID', 1)
	.constant('NIGHTS_GROUP_ID', 2);