/**
 * Master Controller
 */

angular.module('app')
	.controller('QuotationCtrl', 
			['$scope', 
			'$cookieStore', 
			'$http', 
			'API_URL', 
			'PERSONS_GROUP_ID', 
			'NIGHTS_GROUP_ID' , 
			'$httpParamSerializerJQLike', 
			'$uibModal',
			'$location',
			QuotationCtrl
			]);

function QuotationCtrl($scope, 
						$cookieStore, 
						$http, 
						API_URL, 
						PERSONS_GROUP_ID, 
						NIGHTS_GROUP_ID, 
						$httpParamSerializerJQLike, 
						$uibModal,
						$location) 
{
	$scope.quotation = {};
	$scope.quotation.id = 0;
	$scope.quotation.total = 0;
	$scope.quotation.sendEmail = 0;
	$scope.availableoptions = [];
	$scope.persons = 0;
	$scope.nights = 0;
	$scope.selectedPricePerson = 0;
	$scope.selectedPriceNight = 0;
	$scope.popup1 = {
		opened: false
	}; 

	$scope.changeControl = function changeControl(response) {
		var isDifferent = false;
		if ($scope.quotation.id == 0) {
			response = $scope.availableoptions;
		}
		for (var i = 0; i < response.length; i++) {
			var item = response[i];
			if (item.GroupsId == PERSONS_GROUP_ID) {
				if ($scope.persons != (item.selected ? item.selected : 0)) {
					$scope.persons = parseInt((item.selected ? item.selected : 0));
					$scope.selectedPricePerson = ($scope.persons * item.Price);
					isDifferent = true;
				}
			}
			if (item.GroupsId == NIGHTS_GROUP_ID) {
				if ($scope.nights != (item.selected ? item.selected : 0)) {
					$scope.nights = parseInt((item.selected ? item.selected : 0));
					if ($scope.nights > 1) {
						$scope.selectedPriceNight = (($scope.nights - 1) * $scope.selectedPricePerson);
					}
					else {
						$scope.selectedPriceNight = 0;
					}
					isDifferent = true;
				}
			}
		}
		if (isDifferent) {
			if ($scope.quotation.id > 0) {
				updateCurrentOptionsChild(response);
			} else {
				$scope.updateCurrentOptions($scope.persons, $scope.nights);	
			}
			
		}
	}

	$scope.getTotal = function() {
		var total = 0;
		for(var i = 0; i < $scope.availableoptions.length; i++) {
			var item = $scope.availableoptions[i];
			if (item.Type != 'select' && item.selected) {
				total += parseInt((item.selectedPrice ? item.selectedPrice : item.Price));
			}
			else if (item.Type === 'select' && item.selectedObj.Price > 0) {
				total += parseInt(item.selectedObj.Price);
			}
		}
		return total;
	}

	$scope.updateCurrentOptions = function updateCurrentOptions(persons, nights) {
		$http.get(API_URL + "availableoptions?persons=" + parseInt(persons) + "&nights=" + parseInt(nights))
		.success(function(response) {
			updateCurrentOptionsChild(response);
		});
	}

	$scope.updateCurrentOptionsFromId = function updateCurrentOptionsFromId() {
		$http.get(API_URL + "quotations/" + $scope.quotation.id)
			.success(function (response) {
				if (!response) {
					return null;
				}
				$scope.quotation.Name = response.Name;
				$scope.quotation.Surname = response.Surname;
				$scope.quotation.Email = response.Email;
				$scope.quotation.Celular = response.Celular;
				$scope.changeControl(response.items);
			});
	}

	function updateCurrentOptionsChild(response) {
		$scope.availableoptions = [];
		var lastGroupId = 0;
		for (i = 0; i < response.length; i++) {
			var obj = response[i];
		   
			obj.items = [];
			obj.selectedPrice = 0;
			obj.selected = (obj.selected ? obj.selected : "");
			if (obj.Type == 'bool') {
				obj.selected = (obj.selected == "true");
			} else {
				obj.selected = (obj.selected ? obj.selected : "");
			}
			obj.openedPopUp = false;
			obj.selectedObj = {};
			if (obj.GroupsId == PERSONS_GROUP_ID && $scope.persons > 0) {
				obj.selected = $scope.persons;
				obj.selectedPrice = $scope.selectedPricePerson;
			}
			if (obj.GroupsId == NIGHTS_GROUP_ID && $scope.nights > 0) {
				obj.selected = $scope.nights;
				obj.selectedPrice = $scope.selectedPriceNight;
			}
			if (obj.Type != 'select') {
				$scope.availableoptions.push(obj);
			}
			else {

				if (lastGroupId != obj.GroupsId) {
					$scope.availableoptions.push(obj);
				}

				var objClone = deepCopy($scope.availableoptions);
				var obj2 = objClone[$scope.availableoptions.length - 1];
				delete obj.items;
				obj2.items.push(obj);
				if ($scope.quotation.id > 0) {
					obj2.selectedObj = obj;
				}
				$scope.availableoptions[$scope.availableoptions.length - 1] = obj2;
				
			}
			lastGroupId = obj.GroupsId;
		}
	}

	function deepCopy(obj) {
		if (Object.prototype.toString.call(obj) === '[object Array]') {
			var out = [], i = 0, len = obj.length;
			for (; i < len; i++) {
				out[i] = arguments.callee(obj[i]);
			}
			return out;
		}
		if (typeof obj === 'object') {
			var out = {}, i;
			for (i in obj) {
				out[i] = arguments.callee(obj[i]);
			}
			return out;
		}
		return obj;
	}

	$scope.createQuotation = function createQuotation(){
		$scope.quotation.items = [];
		$scope.quotation.total = $scope.getTotal();
		for(var i = 0; i < $scope.availableoptions.length; i++) {
			var item = $scope.availableoptions[i];
			if (item.Type != 'select') {
				$scope.quotation.items.push(item);
			}
			else if (item.Type === 'select') {
				item.selectedObj.selected = item.selectedObj.id;
				$scope.quotation.items.push(item.selectedObj);
			}
		}
	}

	$scope.sendEmail = function sendEmail() {
        $scope.quotation.sendEmail = 1;
        $scope.quotation.sendEmailAdmin = 1;
		$scope.save();
	}

    $scope.sendEmailAdmin = function sendEmailAdmin() {
        $scope.quotation.sendEmailAdmin = 1;
        $scope.save();
    }


    $scope.save = function save() {
        
        var url = API_URL + "availableoptions";
		$scope.createQuotation();
		var myData = $httpParamSerializerJQLike($scope.quotation);
		$http({
			method: 'POST',
			url: url,
			data: myData,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(response) {
			$uibModal.open({
				templateUrl: "myModalContent.html",
				controller: "QuotationModalCtrl",
				controllerAs: "$ctrl"
			});
		}).error(function(response) {
			alert('This is embarassing. An error has occured. Please check the log for details');
		});
	}

	$scope.open1 = function (option) {
		option.openedPopUp = true;
	};

	$scope.options = {
		showWeeks: false
	};

	if ($location.search().id > 0) {
		$scope.quotation.id = $location.search().id;
		$scope.updateCurrentOptionsFromId();
	} else {
		$scope.updateCurrentOptions(15, 3);
	}
	
	$scope.toggleSidebar();
}
