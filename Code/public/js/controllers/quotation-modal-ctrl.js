/**
 * Quotation Modal Controller
 */

angular.module('app')
	.controller('QuotationModalCtrl', 
		['$scope', 
		 '$cookieStore', 
		 '$http', 
		 '$uibModalInstance',
		 QuotationModalCtrl
		 ]);

function QuotationModalCtrl($scope, $cookieStore, $http, $uibModalInstance) 
{
	var $ctrl = this;

	$ctrl.ok = function () {
		$uibModalInstance.close();
		location.reload();
	};

	$ctrl.cancel = function () {
		// $uibModalInstance.dismiss();
		$uibModalInstance.close();
		location.reload();
	};

}
