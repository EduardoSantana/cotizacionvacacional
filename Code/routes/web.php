<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/


//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
	return view('welcome');
});

// Auth::routes();

 // Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
/*
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
*/

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['perms' => 'ver-usuarios'], function (){
	Route::resource('users','UserController');
});

Route::group(['perms' => 'admin-opcion'], function (){
	Route::get('/admin', 'HomeController@appJSadmin')->name('admin');
});

Route::group(['perms' => 'ver-roles'], function (){
	Route::get('roles', 'RoleController@index')->name('roles.index');
	Route::get('roles/create', 'RoleController@create')->name('roles.create');
	Route::post('roles/create', 'RoleController@store')->name('roles.store');
	Route::get('roles/{id}', 'RoleController@show')->name('roles.show');
	Route::get('roles/{id}/edit', 'RoleController@edit')->name('roles.edit');
	Route::patch('roles/{id}','RoleController@update')->name('roles.update');
	Route::delete('roles/{id}', 'RoleController@destroy' )->name('roles.destroy');
});