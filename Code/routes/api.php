<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/v1/availableoptions/{id?}', 'AvailableOptionsController@index');
Route::post('/v1/availableoptions', 'AvailableOptionsController@store');
Route::post('/v1/availableoptions/{id}/sendEmailJS', 'AvailableOptionsController@sendEmailJS');

Route::group(['middleware' => 'jwt.auth'], function ()
{
	Route::get('/auth/user', 'ApiController@getAuthUser');
	Route::get('/v1/quotations/{id?}', 'QuotationsController@index');
	Route::post('/v1/quotations/{id}/sendEmailJS', 'QuotationsController@sendEmailJS');

	Route::get('/v1/options/{id?}', 'OptionsController@index');
	Route::post('/v1/options', 'OptionsController@store');
	Route::post('/v1/options/{id}', 'OptionsController@update');
	Route::post('/v1/options/{id}/delete', 'OptionsController@destroy');

	Route::get('/v1/groups/{id?}', 'GroupsController@index');
	Route::post('/v1/groups', 'GroupsController@store');
	Route::post('/v1/groups/{id}', 'GroupsController@update');
	Route::post('/v1/groups/{id}/delete', 'GroupsController@destroy');
});

Route::post('/auth/login', 'ApiController@login');

