<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Groups') -> insert([
            'Name' => 'Personas',
            'Active' => 1
        ]);
        DB::table('Groups') -> insert([
            'Name' => 'Noches',
            'Active' => 1
        ]);
        DB::table('Groups') -> insert([
            'Name' => 'Plan de Campamento',
            'Active' => 1
        ]);
        DB::table('Groups') -> insert([
            'Name' => 'Plan de Comida',
            'Active' => 1
        ]);
    }
}
