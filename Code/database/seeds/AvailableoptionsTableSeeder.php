<?php

use Illuminate\Database\Seeder;

class AvailableoptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 1,
            'PersonsTo' => 9,
            'NightsFrom' => 1,
            'NightsTo' => 1,
            'Type' => 'int',
            'Price' => 475,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 10,
            'PersonsTo' => 14,
            'NightsFrom' => 1,
            'NightsTo' => 1,
            'Type' => 'int',
            'Price' => 425,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 15,
            'PersonsTo' => 999,
            'NightsFrom' => 1,
            'NightsTo' => 1,
            'Type' => 'int',
            'Price' => 375,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        // // //
        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 1,
            'PersonsTo' => 9,
            'NightsFrom' => 2,
            'NightsTo' => 2,
            'Type' => 'int',
            'Price' => 450,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 10,
            'PersonsTo' => 14,
            'NightsFrom' => 2,
            'NightsTo' => 2,
            'Type' => 'int',
            'Price' => 400,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 15,
            'PersonsTo' => 999,
            'NightsFrom' => 2,
            'NightsTo' => 2,
            'Type' => 'int',
            'Price' => 350,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        // // //
        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 1,
            'PersonsTo' => 9,
            'NightsFrom' => 3,
            'NightsTo' => 99,
            'Type' => 'int',
            'Price' => 450,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 10,
            'PersonsTo' => 14,
            'NightsFrom' => 3,
            'NightsTo' => 99,
            'Type' => 'int',
            'Price' => 400,
            'OrderRank' => 50,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 1,
            'Name' => 'Cantidad de Personas',
            'PersonsFrom' => 15,
            'PersonsTo' => 999,
            'NightsFrom' => 3,
            'NightsTo' => 99,
            'Type' => 'int',
            'Price' => 350,
            'OrderRank' => 50,
            'Active' => 1
        ]);

//* hasta aqui personas *//

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 2,
            'Name' => 'Cantidad de Noches',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'int',
            'Price' => 0,
            'OrderRank' => 60,
            'Active' => 1
        ]);

//* hasta aqui noches *//

        DB::table('AvailableOptions')->insert([
            'Name' => 'Pisina',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'bool',
            'Price' => 2000,
            'OrderRank' => 300,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'Name' => 'Kareokee',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'bool',
            'Price' => 2800,
            'OrderRank' => 310,
            'Active' => 1
        ]);

//* hasta aqui otros *//

        DB::table('AvailableOptions')->insert([
        	'GroupsId' => 3,
            'Name' => 'Apartamento Alquilado',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'select',
            'Price' => random_int(2000, 6000),
            'OrderRank' => random_int(100, 999),
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 3,
            'Name' => 'Lugar Abierto Para Camping',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'select',
            'Price' => random_int(2000, 6000),
            'OrderRank' => random_int(100, 999),
            'Active' => 1
        ]);

//* hasta aqui plan de campamento *//

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 4,
            'Name' => 'Sin Comida',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'select',
            'Price' => 0,
            'OrderRank' => 400,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 4,
            'Name' => 'Renta de Cosina',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'select',
            'Price' => 1500,
            'OrderRank' => 410,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 4,
            'Name' => 'Desayuno y Cena',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'select',
            'Price' => 2500,
            'OrderRank' => 420,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 4,
            'Name' => 'Desayuno, Cena y Comida',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'select',
            'Price' => 3500,
            'OrderRank' => 430,
            'Active' => 1
        ]);

        DB::table('AvailableOptions')->insert([
            'GroupsId' => 4,
            'Name' => 'Desayuno, Cena y Comida',
            'PersonsFrom' => 0,
            'PersonsTo' => 9999,
            'NightsFrom' => 0,
            'NightsTo' => 9999,
            'Type' => 'select',
            'Price' => 4200,
            'OrderRank' => 440,
            'Active' => 1
        ]);

//* hasta aqui plan de comida *//
        
    }
}
