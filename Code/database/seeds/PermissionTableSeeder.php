<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\User;
use App\Role;

class PermissionTableSeeder extends Seeder
{
	/**
	* Run the database seeds.
	* composer dump-autoload
	* php artisan db:seed --class=PermissionTableSeeder
	* @return void
	*/
	public function run()
	{
		$permission = [
			[
				'name' => 'admin-admin',
				'display_name' => 'Administrador de aplicacion',
				'description' => 'Administrador de la aplicacion'
			],
			[
				'name' => 'ver-usuarios',
				'display_name' => 'Ver todos los usuarios',
				'description' => 'Ver todos los usuarios'
			],
			[
				'name' => 'ver-roles',
				'display_name' => 'Ver todos los roles',
				'description' => 'Ver todos los roles'
			]
		];

		foreach ($permission as $key => $value) {
			Permission::create($value);
		}
		
		$activeExist = DB::select("SELECT 1
			FROM information_schema.COLUMNS WHERE UPPER(TABLE_SCHEMA) = UPPER(:dbName)
			AND TABLE_NAME = 'users' 
			AND COLUMN_NAME = 'active'", 
			['dbName' => env('DB_DATABASE', 'dbCotVac')]);
		
		if (empty($activeExist)) {
			DB::statement("ALTER TABLE users ADD COLUMN active INT(1) NOT NULL DEFAULT 0 AFTER password");
		}

		$emailExist = DB::select("select 1 from users where email = 'admin@admin.me'");
		
		if (empty($emailExist)) {
			User::create([
				'name' => 'admin',
				'email' => 'admin@admin.me',
				'password' => bcrypt('admin1234'),
				'active' => 1
			]);
		}

		Role::create([
			'name' => 'admin',
			'display_name' => 'Administrador',
			'description' => 'Administrador de aplicacion'
		]);

		DB::insert('insert into permission_role (role_id,permission_id, created_at) select roles.id, permissions.id,now() from roles join permissions');

		DB::insert('insert into role_user (user_id, role_id, created_at) select users.id, roles.id,now() from users join roles');

	}
}