<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('Quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name');
            $table->string('Surname');
            $table->string('Phone')->nullable();
            $table->string('Celular')->nullable();
            $table->string('Email')->nullable();
            $table->boolean('Confirmed')->nullable();
            $table->decimal('TaxesPorc',4,2);
            $table->decimal('Taxes',16,2);
            $table->decimal('SubTotal',16,2);
            $table->decimal('Total',16,2);
            $table->boolean('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('Quotations');
    }

}