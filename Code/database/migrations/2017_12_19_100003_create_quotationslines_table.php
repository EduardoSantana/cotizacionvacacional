<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationslinesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('QuotationsLines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('QuotationsId');
            $table->integer('AvailableOptionsId');
            $table->string('SelectedValue')->nullable();
            $table->integer('GroupsId')->nullable();
            $table->string('Name');
            $table->string('GroupName')->nullable();
            $table->integer('Qty');
            $table->decimal('Price',16,2);
            $table->decimal('Total',16,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('QuotationsLines');
    }

}