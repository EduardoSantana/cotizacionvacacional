<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableoptionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('AvailableOptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('GroupsId')->nullable();
            $table->string('Name');
            $table->string('Description')->nullable();
            $table->string('Type');
            $table->integer('PersonsFrom');
            $table->integer('PersonsTo');
            $table->integer('NightsFrom');
            $table->integer('NightsTo');
            $table->decimal('Price',16,2);
            $table->integer('OrderRank')->nullable();
            $table->boolean('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('AvailableOptions');
    }

}